import sublime
import sublime_plugin
import os
import xml.etree.ElementTree as ET


class SymfonyTextCommand(sublime_plugin.TextCommand):

    def get_active_view(self):
        return self.view

    def get_active_filename(self):
        return os.path.realpath(self.view.file_name)

    def get_window(self):
        return self.view.window() or sublime.active_window()

    def get_working_dir(self):
        return os.path.realpath(os.path.dirname(self.view.file_name()))

    def root(self):
        try:
            root_dir = self.get_window().folders()[0]
            return root_dir
        except:
            err = 'Try to open folder with Symfony project'
            print(err)


class SymfonyServicesReader:

    def __init__(self, filepath):
        self.filepath = filepath

    def read(self):
        tree = ET.parse(self.filepath)
        root = tree.getroot()
        service_tag = '{http://symfony.com/schema/dic/services}services'
        services = None
        for item in root:
            if (item.tag == service_tag):
                services = list(map(lambda x: x.get('id'), list(item)))
                break
        return services


class SymfonyCache:

    def __init__(self, path, env):
        self.root_dir = path
        self.cache_dir = self.get_dir()
        self.env_dir = self.get_env_dir(env)
        self.services_file = self.file()

    def get_dir(self):
        # Build path of cache dir for Symfony 3.x
        sf3_cache = self.root_dir + os.path.sep + "var"
        sf3_cache = sf3_cache + os.path.sep + "cache"

        # Build path of cache dir for Symfony 2.x
        sf2_cache = self.root_dir + os.path.sep + "app"
        sf2_cache = sf2_cache + os.path.sep + "cache"

        # Check and return path or print error string
        if os.path.isdir(sf3_cache):
            return sf3_cache
        elif os.path.isdir(sf2_cache):
            return sf2_cache
        else:
            err = 'Cache directory not found'
            print(err)

    def get_env_dir(self, env):
        return self.cache_dir + os.path.sep + env

    def file(self):
        filepath = self.env_dir + os.path.sep
        filepath = filepath + "appDevDebugProjectContainer.xml"
        if os.path.isfile(filepath):
            return filepath
        else:
            err = "File with services not found: " + filepath
            print(err)


class SymfonyEditorWriterCommand(sublime_plugin.TextCommand):
    def run(self, edit, text):
        pos = self.view.sel()[0].begin()
        self.view.insert(edit, pos, text)


class SymfonyInsertServiceCommand(SymfonyTextCommand):

    def run(self, edit):
        root_dir = self.root()
        sfCache = SymfonyCache(root_dir, "dev")
        serviceReader = SymfonyServicesReader(sfCache.file())
        services = serviceReader.read()
        self.services = services
        self.view.window().show_quick_panel(services, self.handler)

    def handler(self, item):
        text = self.services[item]
        command = 'symfony_editor_writer'
        if item >= 0:
            self.view.run_command(command, {"text": text})
